
static const char _afb_description_vshl_capabilities[] =
    "{\"openapi\":\"3.0.0\",\"$schema\":\"http://iot.bzh/download/openapi/sch"
    "ema-3.0/default-schema.json\",\"info\":{\"description\":\"\",\"title\":\""
    "High Level Voice Service Capabilities APIs\",\"version\":\"0.1\",\"x-bin"
    "ding-c-generator\":{\"api\":\"vshl-capabilities\",\"version\":3,\"prefix"
    "\":\"afv_\",\"postfix\":\"\",\"start\":null,\"onevent\":null,\"init\":\""
    "init\",\"scope\":\"\",\"private\":false,\"noconcurrency\":true}},\"serve"
    "rs\":[{\"url\":\"ws://{host}:{port}/api/monitor\",\"description\":\"TS c"
    "aching binding\",\"variables\":{\"host\":{\"default\":\"localhost\"},\"p"
    "ort\":{\"default\":\"1234\"}},\"x-afb-events\":[{\"$ref\":\"#/components"
    "/schemas/afb-event\"}]}],\"components\":{\"schemas\":{\"afb-reply\":{\"$"
    "ref\":\"#/components/schemas/afb-reply-v3\"},\"afb-event\":{\"$ref\":\"#"
    "/components/schemas/afb-event-v3\"},\"afb-reply-v3\":{\"title\":\"Generi"
    "c response.\",\"type\":\"object\",\"required\":[\"jtype\",\"request\"],\""
    "properties\":{\"jtype\":{\"type\":\"string\",\"const\":\"afb-reply\"},\""
    "request\":{\"type\":\"object\",\"required\":[\"status\"],\"properties\":"
    "{\"status\":{\"type\":\"string\"},\"info\":{\"type\":\"string\"},\"token"
    "\":{\"type\":\"string\"},\"uuid\":{\"type\":\"string\"},\"reqid\":{\"typ"
    "e\":\"string\"}}},\"response\":{\"type\":\"object\"}}},\"afb-event-v3\":"
    "{\"type\":\"object\",\"required\":[\"jtype\",\"event\"],\"properties\":{"
    "\"jtype\":{\"type\":\"string\",\"const\":\"afb-event\"},\"event\":{\"typ"
    "e\":\"string\"},\"data\":{\"type\":\"object\"}}}},\"responses\":{\"200\""
    ":{\"description\":\"A complex object array response\",\"content\":{\"app"
    "lication/json\":{\"schema\":{\"$ref\":\"#/components/schemas/afb-reply\""
    "}}}}}}}"
;


static const struct afb_verb_v3 _afb_verbs_vshl_capabilities[] = {
    {
        .verb = NULL,
        .callback = NULL,
        .auth = NULL,
        .info = NULL,
        .session = 0,
        .vcbdata = NULL,
        .glob = 0
	}
};

const struct afb_binding_v3 afbBindingV3 = {
    .api = "vshl-capabilities",
    .specification = _afb_description_vshl_capabilities,
    .info = "",
    .verbs = _afb_verbs_vshl_capabilities,
    .preinit = NULL,
    .init = init,
    .onevent = NULL,
    .userdata = NULL,
    .noconcurrency = 1
};

