/*
 * Copyright 2018-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
#include "capabilities/guimetadata/include/GuiMetadataCapability.h"
#include "capabilities/guimetadata/include/GuiMetadataMessages.h"

const string TAG = "vshlcapabilities::capabilities::guimetadata";

using Level = vshlcapabilities::common::interfaces::ILogger::Level;

namespace vshlcapabilities {
namespace capabilities {
namespace guimetadata {

// Create a GuiMetadata.
shared_ptr<GuiMetadata> GuiMetadata::create(
    shared_ptr<vshlcapabilities::common::interfaces::ILogger> logger) {
    auto guiMetadata = std::shared_ptr<GuiMetadata>(new GuiMetadata(logger));
    return guiMetadata;
}

GuiMetadata::GuiMetadata(
    shared_ptr<vshlcapabilities::common::interfaces::ILogger> logger) {
    mLogger = logger;
}

string GuiMetadata::getName() const {
    return NAME;
}

list<string> GuiMetadata::getUpstreamMessages() const {
    return GUIMETADATA_UPSTREAM_ACTIONS;
}

list<string> GuiMetadata::getDownstreamMessages() const {
    return GUIMETADATA_DOWNSTREAM_ACTIONS;
}

}  // namespace guimetadata
}  // namespace capabilities
}  // namespace vshl
