/*
 * Copyright 2018-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
#include "capabilities/navigation/include/NavigationCapability.h"
#include "capabilities/navigation/include/NavigationMessages.h"

const string TAG = "vshlcapabilities::capabilities::navigation";

using Level = vshlcapabilities::common::interfaces::ILogger::Level;

namespace vshlcapabilities {
namespace capabilities {
namespace navigation {

// Create a Navigation.
shared_ptr<Navigation> Navigation::create(
    shared_ptr<vshlcapabilities::common::interfaces::ILogger> logger) {
    auto navigation = std::shared_ptr<Navigation>(new Navigation(logger));
    return navigation;
}

Navigation::Navigation(
    shared_ptr<vshlcapabilities::common::interfaces::ILogger> logger) {
    mLogger = logger;
}

string Navigation::getName() const {
    return NAME;
}

list<string> Navigation::getUpstreamMessages() const {
    return NAVIGATION_UPSTREAM_ACTIONS;
}

list<string> Navigation::getDownstreamMessages() const {
    return NAVIGATION_DOWNSTREAM_ACTIONS;
}

}  // namespace navigation
}  // namespace capabilities
}  // namespace vshl
