/*
 * Copyright 2018-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *     http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

#include "capabilities/CapabilitiesFactory.h"

#include "capabilities/communication/include/PhoneControlCapability.h"
#include "capabilities/guimetadata/include/GuiMetadataCapability.h"
#include "capabilities/navigation/include/NavigationCapability.h"
#include "capabilities/playbackcontroller/include/PlaybackControllerCapability.h"

static string TAG = "vshlcapabilities::core::CapabilitiesFactory";

namespace vshlcapabilities {
namespace capabilities {

// Create CapabilitiesFactory
std::unique_ptr<CapabilitiesFactory> CapabilitiesFactory::create(
    shared_ptr<vshlcapabilities::common::interfaces::ILogger> logger) {
    auto capabilitiesFactory = std::unique_ptr<CapabilitiesFactory>(new CapabilitiesFactory(logger));
    return capabilitiesFactory;
}

CapabilitiesFactory::CapabilitiesFactory(
    shared_ptr<vshlcapabilities::common::interfaces::ILogger> logger) {
    mLogger = logger;
}

std::shared_ptr<common::interfaces::ICapability> CapabilitiesFactory::getGuiMetadata() {
    if (!mGuiMetadata) {
        mGuiMetadata = vshlcapabilities::capabilities::guimetadata::GuiMetadata::create(mLogger);
    }
    return mGuiMetadata;
}

std::shared_ptr<common::interfaces::ICapability> CapabilitiesFactory::getPhoneControl() {
    if (!mPhoneControl) {
        mPhoneControl = vshlcapabilities::capabilities::phonecontrol::PhoneControl::create(mLogger);
    }
    return mPhoneControl;
}

std::shared_ptr<common::interfaces::ICapability> CapabilitiesFactory::getNavigation(
    ) {
    if (!mNavigation) {
        mNavigation = vshlcapabilities::capabilities::navigation::Navigation::create(mLogger);
    }
    return mNavigation;
}

std::shared_ptr<common::interfaces::ICapability> CapabilitiesFactory::getPlaybackController(
    ) {
    if (!mPlaybackController) {
        mPlaybackController = vshlcapabilities::capabilities::playbackcontroller::PlaybackController::create(mLogger);
    }
    return mPlaybackController;
}

}  // namespace capabilities
}  // namespace vshl